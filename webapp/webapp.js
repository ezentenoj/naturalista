/**
 * 
 */
(function(){
	var app = angular.module('demo', ['ngFileUpload']);
	
	app.factory('wikiService', ['$http', function($http){
		var wikiService = {
				get: function(taxon){
					return $http.jsonp('http://es.wikipedia.org/w/api.php?action=query&titles='+taxon+'&prop=pageimages&format=json&pithumbsize=200&redirects&callback=JSON_CALLBACK')
				}
		};
		
		return wikiService;
		
	}]);
	
	app.controller('DemoCtrl', ['$log', '$http', '$scope',
	                            'Upload', 'wikiService',
	                            function($log, $http, $scope,
	                            		Upload, wikiService){
		
		$log.log("Controller");
		
		$scope.image = null;
		$scope.ready = false;
		$scope.predictionImages = [];
		
		$scope.uploadFiles = function(file){
			if (file != null){
				$scope.ready = false;
				$scope.predictionImages = [];
				file.upload = Upload.upload({
					url: 'http://192.168.99.100:5000/upload_file',
					file: file
				});
				
				file.upload.then(function(response){
					fileName = response.data.file_name;
					$http.post('http://192.168.99.100:5000/classify_image',
							{'file_name': fileName}).
							then(function(response){
								$scope.ready = true;
								classNames = response.data['classNames'];
								angular.forEach(classNames, function(val, key){
									angular.forEach(val, function(v, k){
										$scope.predictionImages.push({'name': v});
										wikiService.get(v).then(function(response){
											pages = response.data.query.pages;
											angular.forEach(pages, function(v, k){
												imgUrl = v.thumbnail.source;
												$scope.predictionImages[key].imgUrl = imgUrl;
											});
										});
									});
								});
							})
					
				});
			}
			
		}
		
	}]);
	
})();