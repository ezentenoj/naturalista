'''
Created on Nov 24, 2015

Main file containing all the tasks available to the service.

@author: Daniel
'''
import os
import caffe
import numpy as np
from flask.app import Flask
from flask_cors.extension import CORS
from flask_cors.decorator import cross_origin
from flask.globals import request
from werkzeug.utils import secure_filename
from tempfile import mkdtemp
from flask import jsonify
from naturalistas.api.BirdsCDMX import BirdsCDMX

# Caffe configuration

caffe.set_mode_cpu()

# Googlenet removes the mean for each band. These values
# are well known.

mean = np.array((104.0, 117.0, 123.0), dtype=np.float32)

model_file = os.getenv('MODELFILE', None)
pretrained_file = os.getenv('PRETRAINEDFILE', None)

# Loading net a transformer

net = caffe.Net(model_file, pretrained_file, caffe.TEST)
transformer = caffe.io.Transformer({'data':
                                    net.blobs['data'].data.shape})
transformer.set_transpose('data', (2, 0, 1))
transformer.set_mean('data', mean)
transformer.set_raw_scale('data', 255)
transformer.set_channel_swap('data', (2, 1, 0))

webapp = Flask(__name__)
webapp.debug = True
cors = CORS(webapp)
webapp.config['CORS_HEADERS'] = 'Content-Type'


@webapp.route('/upload_file',
              methods=['POST'])
@cross_origin()
def upload_file():
    """
    Function to upload an image to the server
    """
    f = request.files['file']
    filename = secure_filename(f.filename)
    output_dir = mkdtemp()
    output_file = os.path.join(output_dir, filename)
    f.save(output_file)
    return jsonify({'file_name': output_file})


@webapp.route('/classify_image',
              methods=['POST'])
@cross_origin()
def classify_image():
    """
    Function to classify an image and get the 5th most
    likely classes.
    """
    content = request.get_json()
    file_name = content['file_name']
    img = caffe.io.load_image(file_name)
    # preprocess image
    net.blobs['data'].data[...] = transformer.preprocess('data',
                                                         img)
    # classify image
    out = net.forward()
    output = list()
    top_5 = out['prob'][0].flatten().argsort()[-1:-6:-1]
    classes = np.array(BirdsCDMX.classes)
    for c in classes[top_5]:
        c_dict = {c: BirdsCDMX.classes_dict[c]}
        output.append(c_dict)
    return jsonify({'classNames': output})
