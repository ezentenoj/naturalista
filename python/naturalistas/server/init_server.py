'''
Created on Nov 24, 2015

Main script to start all the services needed for the demo.

@author: Daniel
'''

from paste.translogger import TransLogger
from tasks import webapp
import cherrypy


def run_server():
    """
    Function to start Cherrypy
    """
    app_logged = TransLogger(webapp)
    cherrypy.tree.graft(app_logged, '/')
    cherrypy.config.update({
        'engine.autoreload.on': True,
        'log.screen': True,
        'server.socket_port': 5000,
        'server.socket_host': '0.0.0.0',
        })
    cherrypy.engine.start()
    cherrypy.engine.block()

if __name__ == '__main__':
    run_server()
