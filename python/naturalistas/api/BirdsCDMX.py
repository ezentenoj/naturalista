'''
Created on Sep 23, 2015

Main class to download birds images from Naturalista

@author: Daniel
'''
import requests
import wget
import numpy as np
import os
from skimage.io._io import imread, imsave
from scipy.misc.pilutil import imresize
import shutil
import glob
from random import shuffle


class BirdsCDMX(object):

    classes = ['10274',
               '12714',
               '12727',
               '13858',
               '144455',
               '145221',
               '145245',
               '145289',
               '145308',
               '14912',
               '16447',
               '199840',
               '3017',
               '3544',
               '5715',
               '6141',
               '6930',
               '7266',
               '7576',
               '9100',
               '9607']
    classes_dict = {'10274': 'Pheucticus melanocephalus',
                    '12714': 'Turdus rufopalliatus',
                    '12727': 'Turdus migratorius',
                    '13858': 'Passer domesticus',
                    '144455': 'Ardea alba',
                    '145221': 'Oreothlypis ruficapilla',
                    '145245': 'Setophaga coronata',
                    '145289': 'Melozone fusca',
                    '145308': 'Spinus psaltria',
                    '14912': 'Toxostoma curvirostre',
                    '16447': 'Pyrocephalus rubinus',
                    '199840': 'Haemorhous mexicanus',
                    '3017': 'Columba livia',
                    '3544': 'Columbina inca',
                    '5715': 'Amazilia beryllina',
                    '6141': 'Cynanthus latirostris',
                    '6930': 'Anas platyrhynchos',
                    '7266': 'Psaltriparus minimus',
                    '7576': 'Thryomanes bewickii',
                    '9100': 'Melospiza melodia',
                    '9607': 'Quiscalus mexicanus'}

    def __init__(self):
        """
        Creates the list of taxons based on the cdmx.list file.
        """
        list_file = os.path.dirname(__file__)
        list_file = os.path.join(list_file, 'cdmx.list')
        with open(list_file, 'r') as f:
            self.taxons = f.readlines()
        self.taxons = [int(taxon.replace('\n', '')) for taxon in self.taxons]
        self.url = 'http://conabio.inaturalist.org/observations.json'

    def download_dataset(self, output_dir, max_elements=20):
        """
        Method to download the images from Naturalista.

        :param output_dir: The path of the directory where all the images are
                            going to be stored

        :param max_elements: Specifies how many taxons are going to be kept.
                            These are ordered accordingly to the number of 
                            observations.
        """
        total_elements_per_taxon = list()
        # Download all taxons
        for id_taxon in self.taxons:
            taxon_request = requests.get('http://conabio.inaturalist.org'
                                         '/observations/project/598.json'
                                         '/?taxon_id={0}&'
                                         'quality_grade=research'
                                         .format(id_taxon))
            if taxon_request.status_code == 200:
                total_elements = int(taxon_request.headers['X-Total-Entries'])
                total_elements_per_taxon.append(total_elements)
            else:
                raise Exception("Naturalista returned status code {0}"
                                .format(taxon_request.status_code))
        # Check for most observed taxons
        total_elements_per_taxon = np.array(total_elements_per_taxon)
        indexes_sorted = np.argsort(total_elements_per_taxon)
        indexes_sorted = indexes_sorted[-max_elements:]
        self.taxons = np.array(self.taxons)
        self.taxons = self.taxons[indexes_sorted]

        # Download all taxons
        for id_taxon in self.taxons:
            # check if directory exists. Create it otherwise.
            output_taxon_dir = os.path.join(output_dir,
                                            str(id_taxon))
            if not os.path.isdir(output_taxon_dir):
                os.makedirs(output_taxon_dir)
            taxon_request = requests.get(self.url +
                                         '/?taxon_id={0}&'
                                         'quality_grade=research'
                                         .format(id_taxon))
            if taxon_request.status_code == 200:
                total_elements = int(taxon_request.headers['X-Total-Entries'])
                elements_per_page = int(taxon_request.headers['X-Per-Page'])
                iterations = total_elements // elements_per_page
                iterations = (iterations
                              if total_elements % elements_per_page == 0
                              else iterations + 1)
                for n_iter in xrange(iterations):
                    img_request = requests.get(self.url +
                                               '?taxon_id={0}&'
                                               'quality_grade=research&'
                                               'page={1}'
                                               .format(id_taxon, n_iter + 1))
                    if img_request.status_code == 200:
                        observed_taxons = img_request.json()
                        for observed_taxon in observed_taxons:
                            try:
                                photos = observed_taxon['photos'][0]
                                observed_id = observed_taxon['id']
                                medium_url = photos['medium_url']
                                output = os.path.join(output_taxon_dir,
                                                      str(observed_id)+'.jpg')
                                wget.download(medium_url,
                                              out=output)
                            except Exception:
                                print('An unexpected error happened here')
            else:
                raise Exception("Naturalista returned status code {0}"
                                .format(taxon_request.status_code))

    def resize_dataset(self, input_dir, output_dir):
        """
        Method to resize the dataset. All the images are going to be
        the same.

        :param input_dir: The path of the directory where the original
                        images are

        :param output_dir: The path of the directory where all the
                        resized images are going to be
        """
        for root, dirs, files in os.walk(input_dir):
            for c_dir in dirs:
                dir_to_create = os.path.join(output_dir, c_dir)
                if not os.path.isdir(dir_to_create):
                    os.makedirs(dir_to_create)
            total_images = 0
            for f in files:
                img_file = os.path.join(root, f)
                try:
                    img = imread(img_file)
                    # This value has to change depending on the task
                    img = imresize(img, (256, 256, 3))
                    output_file = os.path.basename(root)
                    output_file = os.path.join(output_dir, output_file, f)
                    imsave(output_file, img)
                    total_images += 1
                    if total_images == 4000:
                        break
                except Exception:
                    pass

    def dataset_to_txt_file(self, input_dir, output_dir):
        """
        This method is useful when using convert_imageset bin from
        Caffe distribution. It will create the train.txt and test.txt
        needed.
        :param input_dir: The path of all the resized images.
        :param output_dir: Where the train.txt and test.txt are going 
            to be created.
        """
        classes = ['10274',
                   '12714',
                   '12727',
                   '13858',
                   '144455',
                   '145221',
                   '145245',
                   '145289',
                   '145308',
                   '14912',
                   '16447',
                   '199840',
                   '3017',
                   '3544',
                   '5715',
                   '6141',
                   '6930',
                   '7266',
                   '7576',
                   '9100',
                   '9607']
        train_files = list()
        test_files = list()
        for current_class in classes:
            current_dir = os.path.join(input_dir, current_class)
            files = glob.glob(os.path.join(current_dir, '*.jpg'))
            shuffle(files)
            # For this example, we are using 3500 image to train and
            # the rest for testing
            selected_train = files[:3500]
            selected_test = files[3500:]
            for t in selected_train:
                img_file = os.path.basename(t)
                train_files.append(os.path.join(current_class, img_file))
            for t in selected_test:
                img_file = os.path.basename(t)
                test_files.append(os.path.join(current_class, img_file))
        output_train_file = os.path.join(output_dir, 'train.txt')
        output_test_file = os.path.join(output_dir, 'test.txt')
        with open(output_train_file, 'w') as o:
            for f in train_files:
                dirname = os.path.dirname(f).replace('/', '')
                c_class = classes.index(dirname)
                o.write('{0} {1}\n'.format(f, c_class))
        with open(output_test_file, 'w') as o:
            for f in test_files:
                dirname = os.path.dirname(f).replace('/', '')
                c_class = classes.index(dirname)
                o.write('{0} {1}\n'.format(f, c_class))

    def move_images(self, input_dir):
        for root, _, files in os.walk(input_dir):
            for f in files:
                img_file = os.path.join(root, f)
                try:
                    _ = imread(img_file)
                    output_file = os.path.join(input_dir, f)
                    shutil.move(img_file, output_file)
                except Exception:
                    pass

if __name__ == '__main__':
    b = BirdsCDMX()
    input_dir = '/Users/Daniel/Documents/CONABIO/Naturalistas/whole_dataset'
    output_dir = ('/Users/Daniel/Documents/CONABIO/Naturalistas/'
                  'whole_dataset_resized')
    b.dataset_to_txt_file(output_dir, output_dir)
